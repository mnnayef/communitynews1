// @flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';
import { type Nyhet, NyhetModel, syncModels } from './models.js';

const public_path = path.join(__dirname, '/../../client/public');

let app = express();

app.use(express.static(public_path));
app.use(express.json()); // For parsing application/json

app.get('/allNews', (req: express$Request, res: express$Response) => {
  return NyhetModel.findAll({
      order: [
        ['priority','ASC'],
        ['createdAt', 'DESC'],
        ['updatedAt', 'DESC'],
      ]
  }).then(nyhet => res.send(nyhet)); //order by priority first -> creating time -> updating time
});





app.get('/allNews/:id', (req: express$Request, res: express$Response) => {
  return NyhetModel.findOne({ where: { id: Number(req.params.id) } }).then(nyhet =>
    nyhet ? res.send(nyhet) : res.sendStatus(404)
  ); //Return 404 if nyhet was not found
});

app.put('/allNews', (req: { body: Nyhet }, res: express$Response) => {
  // Respond with error if id is missing
  if (!req.body.id) return res.sendStatus(400);

  return NyhetModel.update(
    { title: req.body.title, theCase: req.body.theCase, category: req.body.category, priority:req.body.priority },
    { where: { id: req.body.id } }
  ).then(updated => (updated[0] /* affected rows */ > 0 ? res.sendStatus(200) : res.sendStatus(404)));
});
app.delete('/allNews/:id',(req: express$Request, res: express$Response) => {
    // Respond with error if id is missing
    if (!req.params.id) return res.sendStatus(400);
    //delete the case from database
    return NyhetModel.destroy({ where: { id: Number(req.params.id) } });
});

//create a nyhet in dackend
app.post('/allNews', (req: {body:Nyhet},res:express$Response)=>{
  return NyhetModel.create({
      title: req.body.title,
      theCase: req.body.theCase,
      category: req.body.category,
      picture: req.body.picture,
      priority: req.body.priority,
  }).then(() => res.sendStatus(200)).catch(() => res.sendStatus(404)) ;
}
);

// The listen promise can be used to wait for the database connection and web server to start (for instance in your tests)
export let listen = new Promise<void>((resolve, reject) => {
  // Wait for Sequalize to connect to and initialize the database
  syncModels.then(() => {
    let call_listen = reloader => {
      app.listen(3000, (error: ?Error) => {
        console.log(error);
        if (error) reject(error.message);
        console.log('Express server started');
        // Start hot reload (refresh web page on client changes) when not in production environment
        if (reloader) {
          reloader.reload(); // Reload application on server restart
          fs.watch(public_path, () => reloader.reload());
        }
        resolve();
      });
    };
    // Setup hot reload (refresh web page on client changes) when not in production environment,
    // and then start the web server.
    if (process.env.NODE_ENV !== 'production') reload(app).then(reloader => call_listen(reloader));
    else call_listen();
  });
});
