// @flow
import { sharedComponentData } from 'react-simplified';
import axios from 'axios';

export class Nyhet { //The values are given in case the user leaves one of the felts empty {AddCase}
  id: number = 0;
  title: string = 'Untitled';
  theCase: string = 'The case was not Found';
  category: string = 'uncategorized';
  createdAt:string='';
  updatedAt:string='';
  priority: string='2';
  picture: string='https://braou.ac.in/wp-content/themes/braou/images/noimagefound.jpg';
}

class NyhetStore {
  news: Nyhet[] = [];

  currentNyhet: Nyhet = new Nyhet();
  newNyhet:Nyhet=new Nyhet();

  getAllNews() {
    return axios.get<Nyhet[]>('/allNews').then(response => (this.news = response.data)) ;
  }



  getNyhet(id: number) {
    return axios.get<Nyhet>('/allNews/' + id).then(response => {
      this.currentNyhet = response.data;

      // Update the nyheter-array as well in case the row has changed
      let nyhet = this.news.find(nyhet => nyhet.id === this.currentNyhet.id);
      if (nyhet) Object.assign(nyhet, { ...this.currentNyhet }); // Copy data from this.currentNyhet to nyhet
    });
  }
  createNyhet(){
    return axios.post("/allNews", this.newNyhet).then(()  => {
      this.news.push(this.newNyhet);
    });
  }

  updateNyhet() {
    return axios.put('/allNews', this.currentNyhet).then(() => {
      // Update the news-array
      let nyhet = this.news.find(nyhet => nyhet.id === this.currentNyhet.id);
      if (nyhet) Object.assign(nyhet, { ...this.currentNyhet }); // Copy data from this.currentNyhet to news array
    });
  }
  deleteNyhet(id: number){
    return axios.delete('/allNews/'+id).then(()=>{ //delete from database
        let index = this.news.find(nyhet => nyhet.id === this.currentNyhet.id);
        if(index) this.news=this.news.filter(nyhet=>nyhet.id!==index); //delete from news
        //if(index) this.news.splice(index,1);
    });
  }
}
export let nyhetStore = sharedComponentData(new NyhetStore());
