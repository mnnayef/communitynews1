// @flow
import {Component} from "react-simplified";
import {Alert, ButtonDanger} from "./widgets";
import {nyhetStore} from "./stores";
import * as React from "react";



//Changes the current path to a given one
function changepath(newPath:string) {
    location.href=newPath;
}
//to wait between calling two methods
async function init(function1:function,function2:function){
    function1;
    await sleep(2000);
    function2;
}
function sleep(ms:number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
function alertInfo(){Alert.info('Everyone can edit or delete this case');}


/**
 * render a card to show the case with picture on left side and text on right side
 * beside there is also some other functions such as delete/edit the case
 * and all other information like create/update time
 */
export class NyhetDetails extends Component<{ match: { params: { id: number } } }> {

    render() {
        return (
          <div className="card mb-3" style={{maxWidth: '100%',minHeight:'500px',backgroundColor:'#EAEFF4'}} >

                <div className="card-footer">
                    <small className="text-muted">updated at: {nyhetStore.currentNyhet.updatedAt.split('T',1).toString()+' {'+nyhetStore.currentNyhet.updatedAt.substr(11,5)+'}'}</small>
                    <ButtonDanger style={{float:'right'}} onClick={this.remove}>Remove Case</ButtonDanger>
                    <a className="btn btn-primary" href={'/#/allNews/' + nyhetStore.currentNyhet.id + '/edit'} role="button" style={{float:'right',marginRight:'80px', marginLeft:'80px'}}>Edit Case</a>
                    <a href={'/#/'.concat(nyhetStore.currentNyhet.category)} className="btn btn-info" role='button' style={{float:'right'}}>{nyhetStore.currentNyhet.category}</a>
                </div>
                <div className="row no-gutters" style={{minHeight:'430px'}}>
                    <div className="col-md-4">
                        <img src={nyhetStore.currentNyhet.picture} className="card-img" style={{minHeight:'430px'}}/>
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title" style={{fontFamily:'Arial, Helvetica, sans-serif',fontSize:'20px'}} ><b>{nyhetStore.currentNyhet.title}</b></h5>
                            <pre className="card-text" style={{fontFamily:'Arial, Helvetica, sans-serif', fontSize:'16px'}}>{nyhetStore.currentNyhet.theCase}</pre>
                        </div>
                    </div>

                </div>
            </div>
        );
    }

    mounted() {
        nyhetStore.getNyhet(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message)); //Could not find Nyhet
    }
    remove(){
        nyhetStore
            .deleteNyhet(this.props.match.params.id)
            .catch((error: Error) => Alert.danger(error.message));

        function alert(){Alert.success('The case has been successfully Deleted');}
        init(changepath('/'),alert()); //Return to main and show success alert
    }
}