// @flow
import Form from 'react-bootstrap/Form';
import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Alert, NavBar, Card, Row, Column, ButtonDange,CardPicture,NavBarLink} from './widgets';
import { nyhetStore } from './stores';
import { AddCase } from './addCase';
import {NewsList, SportNewsList, TechnologyNewsList, UncategorizedList} from './categories';
import {NyhetEdit} from "./editCase";
import {NyhetDetails} from "./nyhetDetailes";


// Reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let script = document.createElement('script');
  script.src = '/reload/reload.js';
  if (document.body) document.body.appendChild(script);
}

//to change the path of the current path




class Menu extends Component {
  render() {
    return (
      <NavBar brand="Community News">
        <NavBar.Link to="/news">News</NavBar.Link>
        <NavBar.Link to="/sport">Sport</NavBar.Link>
        <NavBar.Link to="/technology">Technology</NavBar.Link>
        <NavBar.Link to="/uncategorized">Uncategorized</NavBar.Link>
        <NavBar.Link to="/addCase">Add Case</NavBar.Link>
          <form className="form-inline" style={{marginLeft:'830px'}}>
              <input className="form-control" type="text" placeholder="Search.." aria-label="Search" />
              <i className="fa fa-search"/>
          </form>
      </NavBar>
    );
  }
}
class Header extends Component{
  render(){
    return(
      <Row>
         <Column  width={2}>
             <NavBarLink to='/'><img src="logo.png" width="150" height="150"/></NavBarLink>
         </Column>
          <Column width={6}>
              <img src="header-image.png" alt="Header image community news" height={150}/>
          </Column>
          <Column >

          </Column>

      </Row>
    );
  }
}

class Livefeed extends Component{
    render(){
        return(
            <marquee width="100%" height="40" style={{border:'solid'}} scrolldelay="60" truespeed="truespeed">
                    {nyhetStore.news.filter(nyhet=>nyhet.priority==='1').map(nyhet=> (
                        <div key={nyhet.id} style={{display:'inline-block', marginRight:'4%'}}>
                          <h5>{nyhet.title}
                            <b style={{color:'#18537B', fontSize:'12px'}}>
                              {'    '+nyhet.createdAt.split('T',1).toString()+' {'+nyhet.updatedAt.substr(11,5)+'} '}
                            </b>
                          </h5>
                        </div>
                    ))}
            </marquee>
        );
    }
    mounted(){
        nyhetStore.getAllNews().catch((error: Error) => Alert.danger(error.message));
    }
}


class Home extends Component {
  render() {
    return(
        <Row>
            {nyhetStore.news.slice(0, 20).map(nyhet=>(  //Slice to limit the number of cases beeing showed
                <Column  width={4} key={nyhet.id}>
                    <CardPicture title={nyhet.title} id={nyhet.id} picture={nyhet.picture}
                                 createdAt={nyhet.createdAt} category={nyhet.category}>
                      {nyhet.theCase}
                    </CardPicture>
                </Column>
            ))
            }

        </Row>
    );
  }
    mounted() {
        nyhetStore.getAllNews().catch((error: Error) => Alert.danger(error.message));
    }
}
class Footer extends Component{
    render(){
        return(
            <footer className="page-footer font-small " style={{backgroundColor:'#18537B',height:'50px', color:'#C7DAE6'}}>
                <div className="footer-copyright text-center py-3">
                    © 2019 Copyright:<a href="/">    communityNews.com</a>
                </div>
            </footer>
        );
    }
}







const root = document.getElementById('root');


if (root)
  ReactDOM.render(
    <HashRouter>
      <div>

        <Header/>
        <Menu />
        <Alert />
        <Route exact path="/" component={Livefeed}/>
        <Route exact path="/news" component={Livefeed}/>
        <Route exact path="/sport" component={Livefeed}/>
        <Route exact path="/technology" component={Livefeed}/>
        <Route exact path="/uncategorized" component={Livefeed}/>
        <Route exact path="/" component={Home} />
        <Route  path="/addCase" component={AddCase} />
        <Route  path="/news" component={NewsList} />
        <Route  path="/sport" component={SportNewsList} />
        <Route  path="/technology" component={TechnologyNewsList} />
        <Route  path="/uncategorized" component={UncategorizedList} />
        <Route  path="/allNews/:id/edit/" component={NyhetEdit} />
        <Route  path="/allNews/:id" component={NyhetDetails} />
        <Footer/>
      </div>
    </HashRouter>,
    root
  );
