// @flow
import {Component} from "react-simplified";
import {CardPicture, Column, Row} from "./widgets";
import {nyhetStore} from "./stores";
import * as React from "react";

export class NewsList extends Component {
    render() {
        return (
          <div className='row' style={{minHeight:'445px'}}>
              {nyhetStore.news.filter(nyhet=>nyhet.category==='news').slice(0,20).map(nyhet=>(
                    <Column right={true} width={4} key={nyhet.id}>
                      <CardPicture title={nyhet.title} id={nyhet.id} picture={nyhet.picture} createdAt={nyhet.createdAt} category={nyhet.category}>{nyhet.theCase}</CardPicture>
                    </Column>
                ))
              }
          </div>
        );
    }
}
export class SportNewsList extends Component {
    render() {
        return (
            <div className='row' style={{minHeight:'445px'}}>
                {nyhetStore.news.filter(nyhet=>nyhet.category=='sport').slice(0, 20).map(nyhet=>(
                    <Column right={true} width={4} key={nyhet.id}>
                        <CardPicture title={nyhet.title} id={nyhet.id} picture={nyhet.picture} createdAt={nyhet.createdAt} category={nyhet.category}>{nyhet.theCase}</CardPicture>
                    </Column>
                ))
                }

            </div>
        );
    }
}
export class TechnologyNewsList extends Component {
    render() {
        return (
            <div className='row' style={{minHeight:'445px'}}>
                {nyhetStore.news.filter(nyhet=>nyhet.category=='technology').slice(0, 20).map(nyhet=>(
                    <Column right={true} width={4} key={nyhet.id}>
                        <CardPicture title={nyhet.title} picture={nyhet.picture} id={nyhet.id} createdAt={nyhet.createdAt} category={nyhet.category}>{nyhet.theCase}</CardPicture>
                    </Column>
                ))
                }

            </div>
        );
    }

}
export class UncategorizedList extends Component {
  render() {
    return (
      <div className='row' style={{minHeight:'445px'}}>
        {nyhetStore.news.filter(nyhet=>nyhet.category==='uncategorized').slice(0, 20).map(nyhet=>(
          <Column right={true} width={4} key={nyhet.id}>
            <CardPicture title={nyhet.title} picture={nyhet.picture} id={nyhet.id} createdAt={nyhet.createdAt} category={nyhet.category}>{nyhet.theCase}</CardPicture>
          </Column>
        ))
        }

      </div>
    );
  }

}