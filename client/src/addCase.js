// @flow
import {Component} from "react-simplified";
import {nyhetStore} from "./stores";
import { Alert, ButtonDanger, ButtonSuccess, Column, Row } from './widgets';
import * as React from "react";

function changepath(newPath:string) {
    location.href=newPath;
}
//to wait between calling two methods

async function init(function1:function,function2:function) {
  function1();
  await sleep(2000);
  function2();
}

function sleep(ms:number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export class AddCase extends Component{
    render(){
        return(
            <div  style={{marginLeft:'0'}}>
                <h1 style={{textAlign:'center', color:'#2F4F4F'}}>Add Case</h1>

                <hr/>
                <form style={{marginLeft:'100px'}}>
                    <div className="form-group">
                        <label aria-label="Large">Title:</label>
                        <input id="title" type="text" className="form-control col-md-5"
                               onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                   nyhetStore.newNyhet.title= event.target.value;}} required/>
                        <small className="form-text text-muted">Choose a title to your Case.</small>
                    </div>

                    <label className="my-1 mr-2" >Category:</label> <br/>
                    <select className="custom-select my-1 mr-sm-2 form-control col-md-5" id="categoryReg"
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                nyhetStore.newNyhet.category= event.target.value;}}
                    >
                        <option value="uncategorized">Choose one...</option>
                        <option value="news">News</option>
                        <option value="sport">sport</option>
                        <option value="technology">Technology</option>
                    </select>

                    <div className="form-group">
                        <label >The case:*</label>
                        <textarea id="theCaseRegister" rows="3" className="form-control col-md-5"
                                  placeholder="Explain.."
                                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                      nyhetStore.newNyhet.theCase= event.target.value;}}
                        />
                    </div>


                    <div className="form-group">
                        <label >Picture:</label>
                        <input id="pictureRegister" type="text" className="form-control col-md-5"
                               onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                   nyhetStore.newNyhet.picture= event.target.value;
                               }}
                        />
                        <small className="form-text text-muted">
                          Enter the link of a picture on Internet. Example: https://www.armaghbanbridgecraigavon.gov.uk/wp-content/uploads/2019/04/sportsballs1.png
                        </small>
                    </div>


                    <div className="form-group" >
                        <label>Priority:</label> <br/>
                        <select id="priorityRegister" className="custom-select form-control col-md-5"
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    nyhetStore.newNyhet.priority= event.target.value;}}>
                            <option value="">Choose one...</option>
                            <option value='1'>1</option>
                            <option value='2'>2</option>
                        </select>
                        <small className="form-text text-muted">where 1 is very important and 2 is
                            less important</small>
                    </div>
                    <br/>

                    <Row>
                        <Column width={4}>
                            <a href="/" className="btn btn-secondary" role="button">Cancel</a>
                        </Column>
                        <Column>
                          <ButtonSuccess onClick={this.save} type="button" className="btn btn-primary">Register</ButtonSuccess>
                        </Column>
                    </Row>
                </form>
              <br/>
            </div>

        );
    }
    save(){
        nyhetStore
            .createNyhet()
            .catch((error:Error)=>Alert.danger(error.message));
        function alert(){Alert.success('your case is successfully saved');}
        init(alert(),changepath('/'));
    }
}


/*<div className="form-group">
                        <div className="form-check">
                            <input className="form-check-input" type="checkbox" value="" id="invalidCheck" required/>
                            <label className="form-check-label" htmlFor="invalidCheck">
                                Agree to terms and conditions
                            </label>
                            <div className="invalid-feedback">
                                You must agree before submitting.
                            </div>
                        </div>
                    </div>
                    <br/>*/