import {Component} from "react-simplified";
import {Alert, ButtonDange, Card, Column, Row} from "./widgets";
import {nyhetStore} from "./stores";
import * as React from "react";
import { createHashHistory } from 'history';
const history = createHashHistory();

export class NyhetEdit extends Component<{ match: { params: { id: number } } }> {
    render() {
        return (
            <Card title="Edit">
                <form>
                    <Row>
                        <Column width={1}>Title</Column>
                        <Column>
                            <input
                                type="text" className="form-control col-md-3"
                                value={nyhetStore.currentNyhet.title}
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    nyhetStore.currentNyhet.title = event.target.value;
                                }}
                            />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={1}>The Case</Column>
                        <Column>
                            <textarea
                                value={nyhetStore.currentNyhet.theCase} className="form-control col-md-3"
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    nyhetStore.currentNyhet.theCase = event.target.value;
                                }}
                            />
                        </Column>
                    </Row>
                    <Row>
                        <Column width={1}>Category</Column>
                        <Column >
                            <select className="custom-select my-1 mr-sm-2 form-control col-md-3"
                                value={nyhetStore.currentNyhet.category}
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    nyhetStore.currentNyhet.category = event.target.value;
                                }}>
                                <option value="news">news</option>
                                <option value="sport">sport</option>
                                <option value="technology">technology</option>
                              <option value="uncategorized">uncategorized</option>
                            </select>
                        </Column>
                    </Row>
                    <Row>
                        <Column width={1}>Priority</Column>
                        <Column>
                            <select id="priorityRegister" className="custom-select mr-sm-2 form-control col-md-3"
                                    value={nyhetStore.currentNyhet.priority}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                        nyhetStore.currentNyhet.priority= event.target.value;}}
                            >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            </select>
                            <small className="form-text text-muted">where 1 is very important and 2 is less important</small>
                        </Column>
                    </Row>
                  <br/>
                    <Row>
                        <Column width={3}>
                          <a href="/" className="btn btn-secondary" role="button">Cancel</a>
                        </Column>
                        <Column width={3}>
                          <ButtonDange.Success onClick={this.save}>Save</ButtonDange.Success>
                        </Column>
                    </Row>
                </form>
            </Card>
        );
    }

    mounted() {

        nyhetStore.getNyhet(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
    }

    save() {
        nyhetStore
            .updateNyhet()
            .then(() => history.push('/allNews/' + nyhetStore.currentNyhet.id))
            .catch((error: Error) => Alert.danger(error.message));
    }
}